﻿using Worker.Models;

namespace Worker.Interfaces
{
    public interface IMessageProducerScopeFactory
    {
        IMessageProducerScope Open(MessageScopeSettings messageScpeSettings);
    }
}
