﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Interfaces
{
    public interface IMessageProducerScope : IDisposable
    {
        IMessageProducer MessageProducer { get; }
    }
}
