﻿using System;
using Worker.QueueServices;

namespace Worker.Interfaces
{
    public interface IMessageConsumerScope : IDisposable
    {
        IMessageConsumer MessageConsumer { get; }
    }
}
