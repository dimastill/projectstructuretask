﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using Worker.Interfaces;
using Worker.Models;

namespace Worker.QueueServices
{
    public class MessageProducerScopeFactory : IMessageProducerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageProducerScope Open(MessageScopeSettings messageScpeSettings)
        {
            return new MessageProducerScope(_connectionFactory, messageScpeSettings);
        }
    }
}
