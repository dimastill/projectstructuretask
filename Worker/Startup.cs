﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Worker.Interfaces;
using Worker.QueueServices;

namespace Worker
{
    class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMessageQueue, MessageQueue>();
        }

        public void Configure(IApplicationBuilder app)
        {
        }
    }
}
