﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        private readonly IQueueService queueService;

        public TeamsController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, 
            IQueueService queueService)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.queueService = queueService;
        }

        // GET: api/Teams
        [HttpGet]
        public IEnumerable<Team> Get()
        {
            queueService.PostValue("Get all teams was triggered");
            return queryProcessor.Process(new GetAllTeamsQuery());
        }

        [Route("oldusers")]
        [HttpGet]
        public IEnumerable<object> GetOldUsers()
        {
            queueService.PostValue("Get old users was triggered");
            return queryProcessor.Process(new GetOldUsersQuery());
        }


        // GET: api/Teams/5
        [HttpGet("{id}", Name = "GetTeam")]
        public Team Get(int id)
        {
            queueService.PostValue("Get team by id was triggered");
            return queryProcessor.Process(new GetTeamQuery { Id = id });
        }
        // POST: api/Teams
        [HttpPost]
        public bool Post([FromBody] AddTeamCommand command)
        {
            queueService.PostValue("Post team was triggered");
            return commandProcessor.Process(command);
        }

        // PUT: api/Teams/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] UpdateTeamCommand command)
        {
            queueService.PostValue($"Post team (id: {id} was triggered");
            command.UpdateId = id;
            return commandProcessor.Process(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            queueService.PostValue($"Delete team (id:{id} was triggered");
            return commandProcessor.Process(new DeleteTeamCommand { Id = id });
        }
    }
}
