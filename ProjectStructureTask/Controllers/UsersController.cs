﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        private readonly IQueueService queueService;

        public UsersController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, 
            IQueueService queueService)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.queueService = queueService;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<User> Get()
        {
            queueService.PostValue("Get all users was triggered");
            return queryProcessor.Process(new GetAllUsersQuery());
        }

        [Route("sorted")]
        [HttpGet]
        public IEnumerable<Services.DTOs.Task> GetSortedUsersList()
        {
            queueService.PostValue("Get sorted users was triggered");
            return queryProcessor.Process(new GetSortedUsersListQuery());
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public User Get(int id)
        {
            queueService.PostValue($"Get user (id:{id}) was triggered");
            return queryProcessor.Process(new GetUserQuery { Id = id });
        }

        [Route("TasksInProjectForUser/{userId}")]
        [HttpGet("{userId}", Name = "GetTaskInProjectForUser")]
        public Dictionary<string, int> GetNumberTasksInProject(int userId)
        {
            queueService.PostValue("Get task in project was triggered");
            return queryProcessor.Process(new GetNumberTasksInProjectQuery { UserId = userId });
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] AddUserCommand command)
        {
            queueService.PostValue("Post user was triggered");
            commandProcessor.Process(command);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UpdateUserCommand command)
        {
            queueService.PostValue("Put user was triggered");
            command.UpdateId = id;
            commandProcessor.Process(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            queueService.PostValue($"Delete user (id:{id}) was triggered");
            commandProcessor.Process(new DeleteUserCommand { Id = id });
        }
    }
}
