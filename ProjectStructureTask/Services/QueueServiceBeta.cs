﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructureTask.Services
{
    public class QueueServiceBeta
    {
        private readonly IConfiguration _config;

        public QueueServiceBeta(IConfiguration config)
        {
            _config = config;
        }

        public bool PostValue(string message)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_config.GetSection(key: "Rabbit").Value)
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare("BetaExchange", ExchangeType.Direct);
                
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(
                    exchange: "BetaExchange",
                    routingKey: "key",
                    basicProperties: null,
                    body: body);

                return true;
            }


        }
    }
}
