﻿using Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    static class Menu
    {
        public static void Display()
        {
            Console.WriteLine("1. Отримати кiлькiсть таскiв у проектi конкретного користувача (по id)\n" +
                              "2. Отримати список таскiв, призначених для конкретного користувача (по id), де name таска " +
                              "<45 символiв\n" +
                              "3. Отримати список (id, name) з колекцiї таскiв, якi виконанi (finished) в поточному (2019) " +
                              "роцi для конкретного користувача (по id)\n" +
                              "4. Отримати список (id, iм'я команди i список користувачiв) з колекцiї команд, учасники яких " +
                              "старшi 12 рокiв, вiдсортованих за датою реєстрацiї користувача за спаданням, а також " +
                              "згрупованих по командах.\n" +
                              "5. Отримати список користувачiв за алфавiтом first_name (по зростанню) " +
                              "з вiдсортованими tasks по довжинi name (за спаданням)\n" +
                              "6. Отримати наступну структуру (передати Id користувача в параметри):\n" +
                              "\tUser\n" +
                              "\tОстаннiй проект користувача(за датою створення)\n" +
                              "\tЗагальна кiлькiсть таскiв пiд останнiм проектом\n" +
                              "\tЗагальна кiлькiсть незавершених або скасованих таскiв для користувача\n" +
                              "\tНайтривалiший таск користувача за датою(найранiше створений - найпiзнiше закiнчений)\n" +
                              "7. Отримати таку структуру (передати Id проекту в параметри):\n" +
                              "\tПроект\n" +
                              "\tНайдовший таск проекту(за описом)\n" +
                              "\tНайкоротший таск проекту(по iменi)\n" +
                              "\tЗагальна кiлькiсть користувачiв в командi проекту, де або опис проекту > 25 символiв, " +
                              "або кiлькiсть таскiв < 3\n" +
                              "8. Вивести лог файл");
        }

        public static int InputMenuItem()
        {
            Console.Write("Виберiть iндекс елемента меню: ");
            int menuItem = Convert.ToInt32(Console.ReadLine());

            return menuItem;
        }

        public static int InputUserId()
        {
            Console.Write("Введiть id користувача: ");
            int userId = Convert.ToInt32(Console.ReadLine());

            return userId;
        }

        public static int InputProjectId()
        {
            Console.Write("Введiть id проекту ");
            int projectId = Convert.ToInt32(Console.ReadLine());

            return projectId;
        }

        public static void DisplayNumberTasksByUserId()
        {
            int userId = InputUserId();

            Dictionary<string, int> numberTasks = Data.GetNumberTasks(userId);

            Console.WriteLine($"Кiлькiсть завдань у проектах для користувача з id {userId}:");
            foreach (var project in numberTasks)
            {
                Console.WriteLine($"Проект: {project.Key} Кiлькiсть таскiв: {project.Value}");
            }
        }

        public static void DisplayListTasksByUserId()
        {
            int userId = InputUserId();

            List<Models.Task> tasks = Data.GetListTasks(userId);

            foreach (var task in tasks)
            {
                Console.WriteLine($"Завдання #{task.Id}: {task.Name}");
            }
        }

        public static void DisplayTasksFinishedIn2019()
        {
            int userId = InputUserId();

            dynamic tasksFinishedIn2019 = Data.GetListTasksFinished2019(userId);
            foreach (var task in tasksFinishedIn2019)
            {
                Console.WriteLine($"Id #{task.Id}. Назва таскiв: {task.Name}");
            }
        }

        public static void DisplayUsersOlderThan12YearsOld()
        {
            foreach (var oldUser in Data.GetOldUsers())
            {
                Console.Write($"Команда #{oldUser.id} {oldUser.name}\nУчасники:\n");
                foreach (var teamUser in oldUser.users)
                {
                    Console.WriteLine($"{teamUser.firstName} {teamUser.lastName} " +
                        $"(Дата народження: {teamUser.birthday.ToString("dd/MM/yyyy")}. " +
                        $"Дата реєстрацiї: {teamUser.registeredAt.ToString("dd/MM/yyyy")})");
                }
            }
        }

        public static void DisplaySortedUsersList()
        {
            foreach (var sortedItem in Data.GetSortedUsersList())
            {
                Console.WriteLine($"{sortedItem.Perfomer.FirstName} {sortedItem.Perfomer.LastName} " +
                                  $"(Таск: {sortedItem.Name})");
            }
        }

        public static void DisplayInfoAboutUser()
        {
            int userId = InputUserId();

            var response = Data.GetInfoAboutTasks(userId);
            var result = response.FirstOrDefault();
            Console.WriteLine($"Користувач #{result.user.id}\n" +
                $"Останнiй проект #{result.lastProject.id}\n" +
                $"(Кiлькiсть таскiв: {result.numberTasksInLastProject}. " +
                $"Незавершених/Скасованих: {result.numberUnfinishedOrCanceledTasks})\n" +
                $"Найтривалiший таск #{result.longTask.id}");
        }

        public static void DisplayInfoAboutProject()
        {
            int projectId = InputProjectId();

            try
            {
                var responseAboutProject = Data.GetInfoAboutProject(projectId);
                var infoAboutProject = responseAboutProject.FirstOrDefault();
                Console.WriteLine($"Проект #{infoAboutProject.project.id}\n" +
                    $"Найдовший таск (за описом) #{infoAboutProject.longDescriptionProject.id}\n" +
                    $"Найкоротший таск (по iменi) #{infoAboutProject.shortNameTask.id}\n" +
                    $"Кiлькiсть користувачiв в командi даного проекту: {infoAboutProject.numberUsersInProject}");
            }
            catch
            {
                Console.WriteLine("Проект не має таскiв");
            }
        }

        public static void DisplayLogs()
        {
            Console.WriteLine("Logs:");
            Data.GetLogs();
        }

        public static void Wait()
        { 
            Console.WriteLine("\nНатиснiть будь-яку клавiшу для повернення до меню");
            Console.ReadKey();
            Console.Clear();
        }

        public static void DisplayCRUD()
        {
            Console.WriteLine("Функiї CRUD:\n1. Create (POST)\n2. Read (GET)\n3. Update (PUT)\n4. Delete (DELETE)");
            
            switch (InputMenuItem())
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;

            }
        }

        public static void DisplayGET()
        {
            Console.WriteLine("1. Teams\n2.Users");
        }
    }
}
