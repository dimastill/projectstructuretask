﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Models
{
    class TaskStateModel
    {
        public long Id { get; set; }
        public string Value { get; set; }
    }
}
