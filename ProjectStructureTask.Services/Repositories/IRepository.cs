﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Repositories
{
    interface IRepository<T> where T : class
    {
        void Add(T item);
        void Delete(int id);
        void Delete(T item);
        T Find(T item);
        T Find(Predicate<T> match);
        List<T> ToList();
        int IndexOf(T item);
    }
}
