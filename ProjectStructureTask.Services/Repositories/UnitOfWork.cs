﻿using ProjectStructureTask.Services.DTOs;
using System;

namespace ProjectStructureTask.Services.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public Repository<Project> Projects { get; } = new Repository<Project>();
        public Repository<Task> Tasks { get; } = new Repository<Task>();
        public Repository<User> Users { get; } = new Repository<User>();
        public Repository<Team> Teams { get; } = new Repository<Team>();

        // Initialization data
        public UnitOfWork()
        {
            Random random = new Random();

            // Generate teams
            for (int i = 0; i < 3; i++)
            {
                var team = new Team()
                {
                    Id = i,
                    CreatedAt = DateTime.Parse($"{random.Next(1, 28)}-{random.Next(1, 12)}-{random.Next(1950, 2020)}"),
                    Name = $"TeamNameTest{i}"
                };

                Teams.Add(team);
            }

            //Generate users
            for (int i = 0; i < 5; i++)
            {
                var user = new User()
                {
                    Id = i,
                    FirstName = $"FirstNameTest{i}",
                    LastName = $"LastNameTest{i}",
                    Email = $"EmailTest{i}@email.com",
                    Birthday = DateTime.Parse($"{random.Next(1, 28)}-{random.Next(1, 12)}-{random.Next(1950, 2020)}"),
                    RegisteredAt = DateTime.Parse($"{random.Next(1, 28)}-{random.Next(1, 12)}-{random.Next(1950, 2020)}"),
                    Team = Teams[random.Next(0, Teams.Count - 1)]
                };

                Users.Add(user);
            }

            //Generate projects
            for (int i = 0; i < 10; i++)
            {
                var project = new Project()
                {
                    Id = i,
                    Name = $"NameTest{i}",
                    CreatedAt = DateTime.Parse($"{random.Next(1, 28)}-{random.Next(1, 12)}-{random.Next(1950, 2020)}"),
                    Deadline = DateTime.Now,
                    Description = $"DescriptionTest{i}",
                    Author = Users[random.Next(0, Users.Count - 1)],
                    Team = Teams[random.Next(0, Teams.Count - 1)]
                };

                Projects.Add(project);
            }

            //Generate tasks
            for (int i = 0; i < 15; i++)
            {
                var task = new Task()
                {
                    Id = i,
                    Description = $"DescriptionTest{i}",
                    Name = $"NameTest{i}",
                    CreateAt = DateTime.Parse($"{random.Next(1, 28)}-{random.Next(1, 12)}-{random.Next(1950, 2020)}"),
                    FinishedAt = DateTime.Now,
                    Perfomer = Users[random.Next(0, Users.Count - 1)],
                    Project = Projects[random.Next(0, Projects.Count - 1)],
                    State = new TaskStateModel() { Id = 0, Value = "Started" }
                };

                Tasks.Add(task);
            }
        }
    }
}
