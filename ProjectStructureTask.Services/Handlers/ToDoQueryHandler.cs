﻿using AutoMapper;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Queries;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoQueryHandler : IToDoQueryHandler
    {
        private readonly UnitOfWork dataContext;
        private readonly IMapper mapper;

        public ToDoQueryHandler(IUnitOfWork dataContext, IMapper mapper)
        {
            this.dataContext = dataContext as UnitOfWork;
            this.mapper = mapper;
        }
        public Dictionary<Type, Func<object, object>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, object>>
            {
                { typeof(GetAllProjectsQuery), t => Handle(t as GetAllProjectsQuery) },
                { typeof(GetAllTasksQuery), t => Handle(t as GetAllTasksQuery) },
                { typeof(GetAllUsersQuery), t => Handle(t as GetAllUsersQuery) },
                { typeof(GetAllTeamsQuery), t => Handle(t as GetAllTeamsQuery) },
                { typeof(GetProjectQuery), t => Handle(t as GetProjectQuery) },
                { typeof(GetTeamQuery), t => Handle(t as GetTeamQuery) },
                { typeof(GetUserQuery), t => Handle(t as GetUserQuery) },
                { typeof(GetTaskQuery), t => Handle(t as GetTaskQuery) },
                { typeof(GetInfoAboutProjectQuery), t => Handle(t as GetInfoAboutProjectQuery) },
                { typeof(GetInfoAboutLastProjectQuery), t => Handle(t as GetInfoAboutLastProjectQuery) },
                { typeof(GetListTasksFinished2019Query), t => Handle(t as GetListTasksFinished2019Query) },
                { typeof(GetListTasksQuery), t => Handle(t as GetListTasksQuery) },
                { typeof(GetNumberTasksInProjectQuery), t => Handle(t as GetNumberTasksInProjectQuery) },
                { typeof(GetOldUsersQuery), t => Handle(t as GetOldUsersQuery) },
                { typeof(GetSortedUsersListQuery), t => Handle(t as GetSortedUsersListQuery) },
                
            };
        }

        private ICollection<Project> Handle(GetAllProjectsQuery query)
        {
            var projects = dataContext.Projects.ToList();
            return projects;
        }

        private ICollection<DTOs.Task> Handle(GetAllTasksQuery query)
        {
            var tasks = dataContext.Tasks.ToList();
            return tasks;
        }

        private ICollection<User> Handle(GetAllUsersQuery query)
        {
            var users = dataContext.Users.ToList();
            return users;
        }

        private ICollection<Team> Handle(GetAllTeamsQuery query)
        {
            var teams = dataContext.Teams.ToList();
            return teams;
        }

        private Project Handle(GetProjectQuery query)
        {
            var project = dataContext.Projects.Find(findProject => findProject.Id == query.ProjectId);
            return project;
        }

        private Team Handle(GetTeamQuery query)
        {
            var team = dataContext.Teams.Find(findTeam => findTeam.Id == query.Id);
            return team;
        }

        private User Handle(GetUserQuery query)
        {
            var user = dataContext.Users.Find(findUser => findUser.Id == query.Id);
            return user;
        }

        private Task Handle(GetTaskQuery query)
        {
            var task = dataContext.Tasks.Find(findTask => findTask.Id == query.Id);
            return task;
        }

        private Dictionary<string, int> Handle(GetNumberTasksInProjectQuery query)
        {
            var resultQuery = dataContext.Projects.GroupJoin(
                dataContext.Tasks.Where(t => t.Id == query.UserId),
                p => p, t => t.Project,
                (project, task) => new
                {
                    project,
                    numberTasks = task.Count(t => t.Project.Id == project.Id)
                });

            Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (var item in resultQuery)
            {
                result.Add(item.project.Name, item.numberTasks);
            }

            return result;
        }

        private List<Task> Handle(GetListTasksQuery query)
        {
            var result = dataContext.Tasks.Where(task => task.Perfomer.Id == query.UserId && task.Name.Length < 45);

            return result.ToList();
        }

        private object Handle(GetListTasksFinished2019Query query)
        {
            var result = dataContext.Tasks.Where(task => task.Perfomer.Id == query.UserId && 
                                                         task.State.Value == "Finished" &&
                                                         task.FinishedAt.Year == 2019)
                                          .Select(task => new { task.Id, task.Name });

            return result;
        }

        private IEnumerable<object> Handle(GetOldUsersQuery query)
        {
            var oldUsers = dataContext.Teams.GroupJoin(
                dataContext.Users.Where(user => user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null)
                                 .OrderBy(user => user.RegisteredAt),
                t => t, u => u.Team,
                (team, user) => new
                {
                    team.Id,
                    team.Name,
                    Users = user.Select(u => u)
                });

            return oldUsers;
        }

        private IEnumerable<Task> Handle(GetSortedUsersListQuery query)
        {
            /*var sortedUsers = from user in dataContext.Users
                              orderby user.FirstName
                              join task in dataContext.Tasks on user.Id equals task.Perfomer.Id
                              orderby task.Name.Length
                              group task by user;*/

            var sortedUsers = dataContext.Tasks.OrderBy(task => task.Name.Length)
                                               .ThenByDescending(task => task.Perfomer.FirstName);

            return sortedUsers;
        }

        private IEnumerable<object> Handle(GetInfoAboutLastProjectQuery query)
        {
            var result = from user in dataContext.Users
                         where user.Id == query.UserId
                         join project in dataContext.Projects on user.Id equals project.Author.Id
                         where project.CreatedAt == dataContext.Projects.Where(p => p.Author.Id == user.Id)
                                                            .Max(x => x.CreatedAt)
                         let lastProject = project
                         join task in dataContext.Tasks on project.Id equals task.Project.Id
                         let numberTasksInLastProject = dataContext.Tasks.Count(t => t.Project.Id == lastProject.Id)
                         let numberUnfinishedOrCanceledTasks = dataContext.Tasks.Count(t => t.Project.Id == lastProject.Id &&
                                                                                   task.State.Value != "Finished")
                         let longTask = dataContext.Tasks.Where(t => t.Project.Id == lastProject.Id)
                                             .Aggregate((x, y) => (x.FinishedAt - x.CreateAt) >
                                                                  (y.FinishedAt - y.CreateAt) ? x : y)
                         select new
                         {
                             user,
                             lastProject,
                             numberTasksInLastProject,
                             numberUnfinishedOrCanceledTasks,
                             longTask
                         };

            return result;
        }

        private IEnumerable<object> Handle(GetInfoAboutProjectQuery query)
        {
            var result = from project in dataContext.Projects
                         where project.Id == query.ProjectId
                         join task in dataContext.Tasks on project.Id equals task.Project.Id
                         let longDescriptionProject = dataContext.Tasks.Where(t => t.Project.Id == project.Id)
                                                           .Aggregate((x, y) => x.Description.Length >
                                                                                y.Description.Length ? x : y)
                         let shortNameTask = dataContext.Tasks.Where(t => t.Project.Id == project.Id)
                                                           .Aggregate((x, y) => x.Name.Length <
                                                                                y.Name.Length ? x : y)
                         let tasksInProject = dataContext.Tasks.Where(t => t.Project.Id == project.Id)
                         let numberUsersInProject = tasksInProject.Where(t => t.Project.Description.Length > 25 ||
                                                                              tasksInProject.Count() < 3)
                                                                  .Select(t => t.Perfomer).GroupBy(t => t.Id).Count()
                         select new
                         {
                             project,
                             longDescriptionProject,
                             shortNameTask,
                             numberUsersInProject
                         };

            return result;
        }
    }
}
