﻿using AutoMapper;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.DTOs;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoCommandHandler : IToDoCommandHandler
    {
        private readonly UnitOfWork dataContext;
        private readonly IMapper mapper;

        public ToDoCommandHandler(IUnitOfWork dataContext, IMapper mapper)
        {
            this.dataContext = dataContext as UnitOfWork;
            this.mapper = mapper;
        }
        public Dictionary<Type, Func<object, object>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, object>>
            {
                { typeof(AddProjectCommand), t => Handle(t as AddProjectCommand) },
                { typeof(AddTeamCommand), t => Handle(t as AddTeamCommand) },
                { typeof(AddUserCommand), t => Handle(t as AddUserCommand) },
                { typeof(AddTaskCommand), t => Handle(t as AddTaskCommand) },
                { typeof(UpdateProjectCommand), t => Handle(t as UpdateProjectCommand) },
                { typeof(UpdateTeamCommand), t => Handle(t as UpdateTeamCommand) },
                { typeof(UpdateUserCommand), t => Handle(t as UpdateUserCommand) },
                { typeof(UpdateTaskCommand), t => Handle(t as UpdateTaskCommand) },
                { typeof(DeleteProjectCommand), t => Handle(t as DeleteProjectCommand) },
                { typeof(DeleteUserCommand), t => Handle(t as DeleteUserCommand) },
                { typeof(DeleteTeamCommand), t => Handle(t as DeleteTeamCommand) },
                { typeof(DeleteTaskCommand), t => Handle(t as DeleteTaskCommand) },
            };
        }

        private bool Handle(AddProjectCommand command)
        {
            if (dataContext.Projects.Find(p => p.Id == command.Id) != null)
                return false;

            var project = mapper.Map<AddProjectCommand, Project>(command);
            dataContext.Projects.Add(project);

            return true;
        }

        private bool Handle(AddTeamCommand command)
        {
            if (dataContext.Teams.Find(t => t.Id == command.Id) != null)
                return false;

            var team = mapper.Map<AddTeamCommand, Team>(command);
            dataContext.Teams.Add(team);

            return true;
        }

        private bool Handle(AddUserCommand command)
        {
            if (dataContext.Users.Find(u => u.Id == command.Id) != null)
                return false;

            var user = mapper.Map<AddUserCommand, User>(command);
            dataContext.Users.Add(user);

            return true;
        }

        private bool Handle(AddTaskCommand command)
        {
            if (dataContext.Tasks.Find(t => t.Id == command.Id) != null)
                return false;

            var task = mapper.Map<AddTaskCommand, Task>(command);
            dataContext.Tasks.Add(task);

            return true;
        }

        private bool Handle(UpdateProjectCommand command)
        {
            var project = dataContext.Projects.Find(updateProject => updateProject.Id == command.UpdateId);

            if (project == null)
                return false;

            mapper.Map(command, project);

            return true;
        }

        private bool Handle(UpdateTeamCommand command)
        {
            var team = dataContext.Teams.Find(updateTeam => updateTeam.Id == command.UpdateId);

            if (team == null)
                return false;

            mapper.Map(command, team);

            return true;
        }

        private bool Handle(UpdateUserCommand command)
        {
            var user = dataContext.Users.Find(updateUser => updateUser.Id == command.UpdateId);

            if (user == null)
                return false;

            mapper.Map(command, user);

            return true;
        }

        private bool Handle(UpdateTaskCommand command)
        {
            var task = dataContext.Tasks.Find(updateTask => updateTask.Id == command.UpdateId);

            if (task == null)
                return false;

            mapper.Map(command, task);

            return true;
        }

        private bool Handle(DeleteProjectCommand command)
        {
            var project = dataContext.Projects.Find(deleteProject => deleteProject.Id == command.ProjectId);

            if (project == null)
                return false;

            dataContext.Projects.Delete(project);

            return true;
        }

        private bool Handle(DeleteTeamCommand command)
        {
            var team = dataContext.Teams.Find(deleteTeam => deleteTeam.Id == command.Id);

            if (team == null)
                return false;

            dataContext.Teams.Delete(team);

            return true;
        }

        private bool Handle(DeleteUserCommand command)
        {
            var user = dataContext.Users.Find(deleteUser => deleteUser.Id == command.Id);

            if (user == null)
                return false;

            dataContext.Users.Delete(user);

            return true;
        }

        private bool Handle(DeleteTaskCommand command)
        {
            var task = dataContext.Tasks.Find(deleteTask => deleteTask.Id == command.Id);

            if (task == null)
                return false;

            dataContext.Tasks.Delete(task);

            return true;
        }
    }
}
