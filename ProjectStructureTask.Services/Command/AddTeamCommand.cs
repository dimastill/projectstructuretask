﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureTask.Services.Abstractions;

namespace ProjectStructureTask.Services.Command
{
    public class AddTeamCommand : ICommand<bool>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
