﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Handlers;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Implementation
{
    public class ToDoQueryProcessor : Processor, IQueryProcessor
    {
        private readonly UnitOfWork context;

        public ToDoQueryProcessor(IToDoQueryHandler handlerFactory)
        {
            RegisterHandlers(handlerFactory);
        }
    }
}
