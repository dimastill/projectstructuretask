﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.DTOs;

namespace ProjectStructureTask.Services.Queries
{
    public class GetTaskQuery : IQuery<Task>
    {
        public int Id { get; set; }
    }
}
