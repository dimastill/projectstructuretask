﻿using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.DTOs;

namespace ProjectStructureTask.Services.Queries
{
    public class GetTeamQuery : IQuery<Team>
    {
        public int Id { get; set; }
    }
}
