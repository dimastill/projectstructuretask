﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface ICommandHandler<in TCommand, TResult> 
        where TCommand : ICommand<TResult> 
        where TResult: new()
    {
        TResult HandleAsync(TCommand command);
    }
}