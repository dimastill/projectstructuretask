﻿using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface ICommandProcessor
    {
        TResult Process<TResult>(ICommand<TResult> command);
    }
}