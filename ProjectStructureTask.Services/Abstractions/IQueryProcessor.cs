﻿using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface IQueryProcessor
    {
        TResult Process<TResult>(IQuery<TResult> query);
    }
}